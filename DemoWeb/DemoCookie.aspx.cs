﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DemoWeb
{
    public partial class DemoCookie : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string nom = txtNom.Text;
            HttpCookie cookie = new HttpCookie("nom", nom);
            cookie.Expires = DateTime.Now.AddDays(30);

            Response.Cookies.Add(cookie);
        }
    }
}