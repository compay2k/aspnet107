﻿<%@ Page Language="C#" %>

<%@ Import Namespace="" %>

<!DOCTYPE html>
<html>
<head>
    <title>ASP.NET Rules</title>
    <link href="css/styles.css" rel="stylesheet" />
</head>
<body>

    <%if (Request.HttpMethod == "GET")
    { 
            %>
        <form method="post" action="inscription.aspx">
            <label for="txtNom">Votre nom : </label>
            <input id="txtNom" name="txtNom" type="text" />
            <br />
            <label for="ddlLangage">langage préféré :</label>
            <select id="ddlLangage" name="ddlLangage">
                <option value="python">Python</option>
                <option value="dotnet">.NET</option>
                <option value="java">Java</option>
            </select>
            <br />
            <input id="chkOptin" name="chkOptin" type="checkbox" />
            <label for="chkOptin">
                J'accepte sans réserve de ne pas vouloir ne pas recevoir
                de mails pour le restant de mes jours
            </label>
            <br />
            <input id="btnMaybe" name="btnMaybe" type="submit" value="Peut-être" />
            <input id="btnOk" name="btnOk" type="submit" value="Valider" />
        </form>
    <%} 

    // je suis en post, donc je récupère les valeurs reçues :
    if (Request.HttpMethod == "POST")
    {
        string nom = Request["txtNom"];
        string langage = Request["ddlLangage"];
    %>
        <h2>Bonjour <%=nom%></h2>
        <p>Vous avez choisi <%=langage%></p>
    <%
    }
    %>

</body>
</html>
